import { createApp } from "vue";
import App from "./App.vue";
import VueTosat from "vue-toast-notification";
import "vue-toast-notification/dist/theme-sugar.css";

createApp(App)
  .use(VueTosat, {
    position: "top-right",
  })
  .mount("#app");
